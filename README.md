# (B)AdBoard
Is a board with a WiFi controller that was hidden in the opaque back of a backpack that I got from ShenZen.

> Advertizing is Bad thing to show on someone's backpack. The board contains firmware that does not allow me to remove the advertisment at the end of the display therefore I looked into how it works.

![Backpack](media/halloween_animation.mp4)

## Specs
- 64x64 matrix of RGB pixels (actually composed of 2 boards 32x64 RGB pixels)
- WiFi controller board RHX8-Q2C
- Android APK RHX Plus to communicate with the controller
- the board draws 5V@up to 1A if some bright animation (gif) plays
- the controller board does not enumerate on USB, just GND and VBUS are connected

## RHX8-Q2C
The main chip appears to be LIGHTNING LN882HK and based on quick search it's ARM
Cortex M4F with some onboard memory and interface for external flash memory
that's probably in the .
It has it's own clock oscillator for the main clock/PLL. Additional chip
GC1302 is there to provide RTC, for that it uses 32kHz crystal RTC and is backed by small battery.

On the IO side there are two MW245B chips that represent 3-state output
eight-way signal transceiver that then go to 2 2x8 parallel wire connectors
that are attached to the matrix.


![RHX8](media/RHX8.bmp)
![GC1302](media/GC1302.bmp)
![LN882H Overview](media/LN882H.png)

## [RHX Plus](https://play.google.com/store/apps/details?id=com.rhxled.led_cp&hl=en_US)
Android Aplication, requires the user is connected to the Access Point the
RHX8-Q2C controllers creates.
It selects the right controller and allows to operate either in text mode or in picture mode.
GIFs are supported.
There's limitation to store just 10 pictures on the board - maybe due to size of the internal Flash memory.

![RHXPLUS](media/RHXPLUS.webp)

## LED Matrix
The individual RGB LED matrix appears to as a node that can be chained as
there is connector labeled as INPUT and OUTPUT.

![Matrix](media/matrix_off.jpg)

## Backpack
The LED board was in a back pocket of a pocket with opaque back.

![Bag](media/bag.jpg)
![Bag with pocket](media/bag_pocket.jpg)

## Links

- [RGB Matrix 64x32 pixels](https://www.waveshare.com/wiki/RGB-Matrix-P3-64x32)
- [PIXELIX](https://github.com/BlueAndi/esp-rgb-led-matrix)
- [Pixel project](https://enesdemirag.github.io/pixel-project/)
- [Smart Matrix Library](https://hackaday.io/project/148066-smartmatrix-library-esp32-port)
- [Galagino](https://github.com/harbaum/galagino)

## License
This project uses the [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/project/cernohl/-/wikis/Documents/CERN-OHL-version-2).
