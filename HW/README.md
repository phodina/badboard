# (B)Ad Board

This is LED matrix controller powered from USB that can load data to the matrix from various sources:

- USB
- uSD card
- Wifi
- Bluetooth

![Bad Board](data/Badboard_3d_top_R1.png)

The main purpose is to replace the prorietary controller by Open Hardware and Software.

Additionaly this board offers input from Nintendo Wii Nunchuck which can be used for games such as:

- Pacman
- Pong
- Doom

